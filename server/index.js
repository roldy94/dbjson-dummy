
const express = require('express');
const myJson = require('../api/db.json')

const app = express();
const port = 8080; // default port to listen

// define a route handler for the default home page
app.get('/', (req, res) => {
  res.send('Hello world!');
});
app.get('/products', (req, res) => {
  res.send(myJson.products);
});
// start the Express server
app.listen(port, () => {
  console.log(`server started at http://localhost:${port}`);
});
