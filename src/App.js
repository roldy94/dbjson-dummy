import './App.css';
import Products from "./componentes/products";

function App() {
  return (
    <div className="App">
      <Products/>
    </div>
  );
}

export default App;
