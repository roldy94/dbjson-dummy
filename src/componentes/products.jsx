import { useEffect, useState } from 'react';

const URL = 'http://localhost:3000/products';

const Products = () => {

  const [data, setData] = useState([]);
  const [text, setText] = useState('');
  
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(URL);
        const json = await response.json();
        setData(json);
      } catch (error) {
        console.log('error', error);
      }
    };
    fetchData();
    // console.log(myJSON);
  }, []);

  const search = (e) => {
    setText(e.target.value);
    const fetchData = async () => {
      try {
        let response = null;
        if (e.target.value !== '') {
          response = await fetch(`${URL}?brand=${e.target.value}`);
        } else {
          response = await fetch(URL);
        }
        const json = await response.json();
        setData(json);
      } catch (error) {
        console.log('error', error);
      }
    };
    fetchData();
  };

  return (
    <div>
      <h2>DB JSON Dummy</h2>
      <input
        type={'text'}
        value={text}
        onChange={search}
        style={{ width: '25%', height: '2rem' }}
      />
      {data.length !== 0
        ? data.map((item, i) => {
            return <p key={i}>{`${item.category} ${item.brand} `}</p>;
          })
        : null}
    </div>
  );
};

export default Products;
